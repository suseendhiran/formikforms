import React from 'react';
import { useFormik, Formik } from 'formik'

const initialValues = {
    name: 'Susee',
    email: '',
    channel: ''
}
const onSubmit = values => {
    console.log('Form data : ', values)
}

const validate = values => {
    let errors = {};

    if (!values.name) {
        errors.name = 'Required'
    }
    if (!values.email) {
        errors.email = 'Required'
    }
    else if (!/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/i.test(values.email)) {
        errors.email = 'Invalid Email address'
    }
    if (!values.channel) {
        errors.channel = 'Required'
    }

    return errors
}


function Formcomp() {

    const formik = useFormik({
        initialValues,
        onSubmit,
        validate,

    })

    console.log(formik.values)
    //console.log('Form errors : ', formik.errors)
    //console.log('Visited fields : ', formik.touched)
    return (
        <div>
            <form onSubmit={formik.handleSubmit}>
                <div>
                    <label htmlFor='name'>Name</label>
                    <input type='text' onChange={formik.handleChange} onBlur={formik.handleBlur} value={formik.values.name} name='name' id='name' />
                    {formik.touched.name && formik.errors.name ? <span>{formik.errors.name}</span> : null}
                </div>
                <div>
                    <label htmlFor='email'>Email</label>
                    <input type='email' onChange={formik.handleChange} onBlur={formik.handleBlur} value={formik.values.email} id='email' name='email' />
                    {formik.touched.email && formik.errors.email ? <span>{formik.errors.email}</span> : null}
                </div>
                <div>
                    <label htmlFor='channel'>Channel</label>
                    <input type='text' onChange={formik.handleChange} onBlur={formik.handleBlur} value={formik.values.channel} id='channel' name='channel' />
                    {formik.touched.channel && formik.errors.channel ? <span>{formik.errors.channel}</span> : null}
                </div>
                <button type='submit'>Submit</button>
            </form>
        </div>
    )
}

export default Formcomp