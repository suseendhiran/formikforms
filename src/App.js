import React from 'react';
import logo from './logo.svg';
import './App.css';
import Formcomp from './Components/Formcomp';

function App() {
  return (
    <div className="App">
      <Formcomp />
    </div>
  );
}

export default App;
